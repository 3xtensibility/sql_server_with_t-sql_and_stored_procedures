use call_center_database;
GO

exec('create view pto_v as
	select * from pto;')

exec('create procedure agent_availability_procedure
as
begin
	insert into agent_availability_que (roster_employee_id)
	select ro.employee_id
	from roster as ro
	where len(ro.team_lead) > 0 except
		(select aq.roster_employee_id
		from agent_availability_que as aq)
		union all
		(select roster_employee_id
		from pto_v
		where CURRENT_TIMESTAMP < last_date_pto);	
end')

exec('create trigger agent_availability_trigger
on call_center_database.dbo.agent_availability_que
after insert
as
begin
	update agent_availability_que set time_available = CURRENT_TIMESTAMP
		where time_available IS NULL;
end')

exec('create trigger incoming_calls_trigger
on call_center_database.dbo.queuing_calls
after insert
as
begin
	delete from ongoing_call where time_initiated < CURRENT_TIMESTAMP and call_status = ''completed'';

	insert into ongoing_call (call_id_num, phone_number, roster_employee_id, call_status, time_initiated)
		select qc.call_id_num,qc.phone_number, aq.roster_employee_id,
			qc.call_status, qc.time_initiated
			from queuing_calls as qc inner join 
			agent_availability_que as aq on qc.call_id_num = aq.call_id_num
			where qc.call_id_num = aq.call_id_num;

	update ongoing_call set call_status = ''servicing'' where call_status = ''waiting'';

	delete from queuing_calls where call_status = ''waiting'' and time_initiated < CURRENT_TIMESTAMP;

	delete from agent_availability_que where roster_employee_id in
		(select roster_employee_id from ongoing_call);
end')

exec('create procedure team_lead_availability_procedure
as
begin
	insert into team_lead_que (roster_employee_id)
		select tx.employee_id from (select employee_id from roster where len(roster.team_lead) = 0
		except select roster_employee_id
		from team_lead_que) as tx except
		select roster_employee_id from pto_v where CURRENT_TIMESTAMP < last_date_pto;
end')

exec('create trigger team_lead_availability_trigger
on call_center_database.dbo.team_lead_que
after insert
as
begin
	update team_lead_que set time_available = CURRENT_TIMESTAMP
		where time_available IS NULL;
end')

exec('create trigger randomize_escalated_calls_trigger
on call_center_database.dbo.ongoing_call
after insert
as
begin
	update ongoing_call set call_status = ''escalated'' where CHARINDEX(''9'',convert(varchar, interaction_num)) > 0;
end')

exec('create trigger escalated_calls_trigger
on call_center_database.dbo.ongoing_call
after update
as
begin
	insert into escalated_call_que (phone_number, call_status, time_initiated)
		select phone_number, call_status, time_initiated from ongoing_call where call_status = ''escalated'';

	delete from ongoing_call where call_status = ''escalated'';
end')

exec('create procedure call_completed_procedure
as
begin
	update ongoing_call set call_status = ''completed'';
end')

exec('create trigger ongoing_calls_trigger
on call_center_database.dbo.ongoing_call
after update
as
begin
	begin try
	insert into call_history (employee_id_time_initiated_composite, insert_date, phone_number, roster_employee_id,
		call_id_num, interaction_num, call_status, time_initiated)
		select trim(roster_employee_id + convert(varchar(100), time_initiated, 20)) as employee_id_time_initiated_composite,
			CURRENT_TIMESTAMP, phone_number, roster_employee_id, call_id_num, interaction_num, call_status,
			time_initiated
			from ongoing_call
			where call_status = ''completed'';
	end try
	begin catch
	if @@trancount > 0
		rollback tran
	end catch

	delete from ongoing_call where call_status = ''completed'';
end')

exec('create trigger escalated_ongoing_calls_trigger
on call_center_database.dbo.ongoing_call
after insert
as
begin
	insert into escalated_ongoing_call (call_id_num2, phone_number, roster_employee_id, call_status, time_initiated)
		select ec.call_id_num2,ec.phone_number, tq.roster_employee_id,
			ec.call_status, ec.time_initiated
			from escalated_call_que as ec inner join 
			team_lead_que as tq on ec.call_id_num2 = tq.call_id_num2
			where ec.call_id_num2 = tq.call_id_num2;

	update ongoing_call set call_status = ''servicing'' where call_status = ''waiting'';

	delete from escalated_call_que where call_status = ''escalated'' and time_initiated < CURRENT_TIMESTAMP;

	delete from team_lead_que where roster_employee_id in
		(select roster_employee_id from escalated_ongoing_call);
end')

exec('create procedure escalated_call_completed_procedure
as
begin
	update escalated_ongoing_call set call_status = ''completed'' where call_status = ''escalated'';
end')

exec('create trigger escalated_ongoing_calls_history_trigger
on call_center_database.dbo.escalated_ongoing_call
after update
as
begin
	insert into call_history (employee_id_time_initiated_composite, insert_date, phone_number, roster_employee_id,
		call_id_num, interaction_num, call_status, time_initiated)
		select trim(roster_employee_id + convert(varchar(100), time_initiated, 20)) as employee_id_time_initiated_composite,
			CURRENT_TIMESTAMP, phone_number, roster_employee_id, call_id_num2, interaction_num2, call_status,
			time_initiated
			from escalated_ongoing_call
			where call_status = ''completed'';

	delete from escalated_ongoing_call where call_status = ''completed'';
end')

exec('create procedure pto_taken_procedure
as
begin
	update pto set last_date_pto = DATEADD(day,2,last_date_pto) where pto.roster_employee_id =
		(select oc.roster_employee_id
		from ongoing_call as oc inner join call_history as ch on oc.interaction_num = ch.interaction_num
		where oc.phone_number = ch.phone_number and oc.roster_employee_id <> ch.roster_employee_id
		group by oc.roster_employee_id
		having count(oc.roster_employee_id) > 1);

	delete from agent_availability_que where roster_employee_id = 
		(select oc.roster_employee_id
		from ongoing_call as oc inner join call_history as ch on oc.interaction_num = ch.interaction_num
		where oc.phone_number = ch.phone_number and oc.roster_employee_id <> ch.roster_employee_id
		group by oc.roster_employee_id
		having count(oc.roster_employee_id) > 1);

	insert into agent_availability_que (roster_employee_id)
		select ptv.roster_employee_id
		from pto_v as ptv
		where ptv.last_date_pto < CURRENT_TIMESTAMP;
end')

exec('create view total_calls_per_call_center_view
as (select top 100 percent count(ch.roster_employee_id) as count_of_escalated_calls,
		ro.call_center as call_center
	from call_history as ch inner join roster as ro on ch.roster_employee_id = ro.employee_id
	where insert_date between ''2021-01-01'' and ''2021-12-31''
	group by ro.call_center
	order by count_of_escalated_calls desc)
')

exec('create view total_calls_per_region_view
as (select top 100 percent count(ch.roster_employee_id) as count_of_escalated_calls,
		ro.region as region, ro.call_center as call_center
	from call_history as ch inner join roster as ro on ch.roster_employee_id = ro.employee_id
	where insert_date between ''2021-01-01'' and ''2021-12-31''
	group by ro.region, ro.call_center
	order by count_of_escalated_calls desc)')

exec('create view escalated_calls_team_lead_view
as (select top 100 percent count(ch.roster_employee_id) as count_of_escalated_calls, max(ro.team_member) as team_lead_name,
		max(ro.call_center) as call_center, max(ro.region) as region,
		max(ro.pod) as pod, max(ro.e_start_date) as employee_start_Date
	from call_history as ch inner join roster as ro on ch.roster_employee_id = ro.employee_id
	where ro.position = ''Team Lead'' and insert_date between ''2021-01-01'' and ''2021-12-31''
	group by ch.roster_employee_id
	order by count_of_escalated_calls desc)')

exec('create view escalated_calls_agent_view
as (select top 100 percent count(ch.roster_employee_id) as count_of_escalated_calls, max(ro.team_member) as team_lead_name,
		max(ro.call_center) as call_center, max(ro.region) as region,
		max(ro.pod) as pod, max(ro.e_start_date) as employee_start_Date
	from call_history as ch inner join roster as ro on ch.roster_employee_id = ro.employee_id
	where ro.position <> ''Team Lead'' and insert_date between ''2021-01-01'' and ''2021-12-31'' and
		ch.phone_number in (select ch.phone_number
								from call_history as ch inner join roster as ro on ch.roster_employee_id = ro.employee_id
								where ro.position = ''Team Lead'' and insert_date between ''2021-01-01'' and ''2021-12-31'')
	group by ch.roster_employee_id
	order by count_of_escalated_calls desc)')

exec('create view escalated_calls_call_center_view
as (select top 100 percent count(ch.roster_employee_id) as count_of_escalated_calls,
		ro.call_center as call_center, ro.region as region, ro.pod as pod
	from call_history as ch inner join roster as ro on ch.roster_employee_id = ro.employee_id
	where ro.position <> ''Team Lead'' and insert_date between ''2021-01-01'' and ''2021-12-31''
	group by ro.call_center, ro.region, ro.pod
	order by count_of_escalated_calls desc)')

exec('create view escalated_calls_region_view
as (select top 100 percent count(ch.roster_employee_id) as count_of_escalated_calls,
		ro.region as region, max(ro.call_center) as call_center, ro.pod as pod
	from call_history as ch inner join roster as ro on ch.roster_employee_id = ro.employee_id
	where ro.position <> ''Team Lead'' and insert_date between ''2021-01-01'' and ''2021-12-31''
	group by ro.region, ro.pod
	order by count_of_escalated_calls desc)')

exec('create view per_hour_wage_call_center
as (select top 100 percent cast(sum(ps.pay) as money) as per_hour_wage_rate, ro.call_center as call_center,
		dense_rank() over(order by sum(ps.pay) desc) [rank], ntile(4) over(order by sum(ps.pay) desc) [row_number]
	from roster as ro inner join  positions as ps on ro.position = ps.position_level
	group by ro.call_center
	order by sum(ps.pay) desc)
')

exec('create view per_hour_wage_region
as (select top 100 percent cast(sum(ps.pay) as money) as per_hour_wage_rate, ro.region as region, ro.call_center as call_center,
		dense_rank() over(order by sum(ps.pay) desc) as rank, ntile(4) over(order by sum(ps.pay) desc) as quartile
	from roster as ro inner join  positions as ps on ro.position = ps.position_level
	group by ro.region, ro.call_center
	order by sum(ps.pay) desc)')

exec('create view per_hour_wage_window_by_call_center
as (select top 100 percent cast(sum(ps.pay) as money) as per_hour_wage_rate, ro.call_center as call_center, ro.region as region,
		dense_rank() over(partition by ro.call_center order by sum(ps.pay) desc) as rank,
		ntile(4) over(order by sum(ps.pay) desc) as quartile
	from roster as ro inner join  positions as ps on ro.position = ps.position_level
	group by ro.call_center, ro.region
	order by ro.call_center, sum(ps.pay) desc)')

exec('create view team_position_seniority
as (select top 100 percent ro.call_center, ro.region, ro.pod, ro.position, 
		count(ro.employee_id) as position_title_count,
		rank() over(partition by ro.position order by count(ro.employee_id) desc) as position_type_rank,
		ntile(4) over(partition by ro.position order by count(ro.employee_id) desc) as quartile
	from roster as ro
	group by ro.call_center, ro.region, ro.pod, ro.position
	order by ro.call_center, ro.region, ro.pod, ro.position desc)')

exec('create view agent_schedule_adherence
as (select top 100 percent ch.roster_employee_id as employee_id,
		sum(round(cast((aq.time_available - ch.insert_date) as float), 6)) as agent_schedule_adherence
	from call_history as ch inner join agent_availability_que as aq on ch.roster_employee_id = aq.roster_employee_id
	where aq.roster_employee_id in (select r.employee_id from roster r where len(r.team_lead) <> 0)
	group by ch.roster_employee_id
	order by ch.roster_employee_id)')

declare @counter int
set @counter = 1
while (@counter <=100)
begin
	exec('agent_availability_procedure')
	exec('team_lead_availability_procedure')
	exec('call_completed_procedure')
	exec('escalated_call_completed_procedure')
	insert into queuing_calls (phone_number, call_status, time_initiated)
		values (concat_ws('-','801',right('000'+cast((FLOOR(RAND()*(999-0+1))+0) as varchar(3)),3),right('0000'+cast((FLOOR(RAND()*(9999-0+1))+0) as varchar(4)),4)),'waiting',CURRENT_TIMESTAMP);
	exec('pto_taken_procedure')
	set @counter = @counter + 1
end;

Welcome to the SQL_Server_With_T-SQL_And_Stored_Procedures repository of Blake Giove.


PURPOSE FOR THE REPOSITORY
To show skills in T-SQL and Stored Procedures used to automate an SQL Server database (specifically, the Transform step in an ETL process).
The database is built utilizing SQL Server 2019 Developer version for code portability.


REPOSITORY EXICUTION PROCESS
The INSTRUCTIONS.txt file shows the four steps to take in order to build the SQL Server database for a call center for this repository.
The database is set up to be a Star Schema and made as simple as possible to allow for ease of recreation.


ADDITIONAL NOTES
The technology stack includes: Windows 10, SQL Server 2019 Developer Edition (64-bit) version 15.0.2000.5, Microsoft SQL Server Management Studio version 15.0.18384.0, and Excel in Microsoft 365 version 2106 (Build 14131.20278).
The SQL Server, Microsoft SQL Server Management Studio, and Excel in Microsoft 365 on Windows 10 should be installed prior to utilizing the repository.

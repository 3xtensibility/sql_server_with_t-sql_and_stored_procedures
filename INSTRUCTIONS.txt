INSTRUCTIONS FOR SQL_Server_With_T-SQL_And_Stored_Procedures REPOSITORY
There are two types of files that can be used to build the database: .sql (SQL) files that can be automatically loaded within Microsoft SQL Server Management Studio
and .txt (text) files that can be copied and pasted into a new query.
Both types of files when executed in order will create the same results.
The two steps to build the Star Schema call_center_database are as follows:


FIRST STEP: run the SQL_Server_Call_Center_Database_Setup.sql file
(or copy and past then run the SQL_Server_Call_Center_Database_Setup.txt file)


SECOND STEP: run the SQL_Server_Stored_Procedures_And_Analytics.sql file
(or copy and past then run the SQL_Server_Stored_Procedures_And_Analytics.txt file)


ADDITIONAL INFORMATION:
If you want to remove the database, schema, and user roles and permissions for any reason, the following commands will do so:

use master;
drop database call_center_database;
drop user call_center_admin;
drop login call_center_admin;
select * from INFORMATION_SCHEMA.SCHEMATA where SCHEMA_OWNER = 'call_center_admin';
select dbname from sys.syslogins where sys.syslogins.dbname = 'call_center_database';
select name from sys.server_principals where sys.server_principals.name = 'call_center_admin';
